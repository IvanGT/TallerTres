﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ttres.Models
{
    class Usuarios
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id_u { get; set; }
        // el nombre no mayor de 150 digitos
        [MaxLength(150)]
        public string UserName { get; set; }

        public string Password { get; set; }

        public string Avatar { get; set; }

        public string Estado { get; set; }

    }
}
