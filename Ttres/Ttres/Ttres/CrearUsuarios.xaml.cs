﻿using Ttres.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ttres
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CrearUsuarios : ContentPage
	{
		public CrearUsuarios ()
		{
			InitializeComponent ();
		}


        async private void guardarr(object sender, EventArgs e)
        {
            int selectedIndex = EntryEstado.SelectedIndex;

            if ((string.IsNullOrEmpty(EntryName2.Text)) || string.IsNullOrEmpty(EntryPassword.Text) ||
                    string.IsNullOrEmpty(EntryAvatar.Text) || (selectedIndex == -1))
            {


                await DisplayAlert("Error", "Debe Completar Los Campos", "Ok");

            }
            else
            {

                await DisplayAlert("", "Si FUe CReada", "");


            }
        }


        public void SaveUsuar(object sender, EventArgs e)
        {
            int sel = EntryEstado.SelectedIndex;

            Usuarios usuario = new Usuarios()
            {
                UserName = EntryName2.Text,
                Password = EntryPassword.Text,
                Avatar = EntryAvatar.Text,
                Estado = EntryEstado.Items[sel]

            };
            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {
                // crear una tabla en base de datos.
                connection.CreateTable<Usuarios>();
                // crear registro  en la tabla "tarea"
                var result = connection.Insert(usuario);
                if (result > 0)

                {
                    DisplayAlert("correcto", "la tarea se creo correctamente", "ok");
                }
                else
                {
                    DisplayAlert("Error", "La Tarea No Fue Creada", "ok");
                }

            }

        }
        async private void ListUsuar(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ListarUsuarios());
        }
    }
}