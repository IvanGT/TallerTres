﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ttres.Models
{
    class Personas
    {
        
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id{ get; set; }
        // el nombre no mayor de 150 digitos
        [MaxLength(150)]
        public string Name { get; set; }

        public string Telephone { get; set; }

        public string Email { get; set; }

        public string Sexo { get; set; }
    }
}

