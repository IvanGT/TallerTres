﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Ttres
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}
        async private void ValidateUser(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CrearPeronas());
        }
        async private void ValidatePerson(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CrearUsuarios());
        }

    }
}
