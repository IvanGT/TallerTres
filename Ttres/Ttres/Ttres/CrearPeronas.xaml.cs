﻿using Ttres.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ttres
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CrearPeronas : ContentPage
    {
        public CrearPeronas()
        {
            InitializeComponent();
        }
        async private void guardar(object sender, EventArgs e)
        {
            int selectedIndex = EntryGender.SelectedIndex;

            if((string.IsNullOrEmpty(EntryName.Text)) || string.IsNullOrEmpty(EntryTelephone.Text)||
                    string.IsNullOrEmpty(EntryEmail.Text) || (selectedIndex == -1))
                    {
                        

                        await DisplayAlert("Error","Debe Completar Los Campos","Ok");

                     }else{

                         await DisplayAlert("","Si FUe CReada","");

                
            }
         }
        public void ListPersons(object sender, EventArgs e)
        {
            int SelectedIndex = EntryGender.SelectedIndex;
            Personas crear = new Personas()
            {
                Name = EntryName.Text,
                Telephone = EntryTelephone.Text,
                Email = EntryEmail.Text,
                Sexo = EntryGender.Items[SelectedIndex]
            
            };
            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {
                // crear una tabla en base de datos.
                connection.CreateTable<Personas>();
                // crear registro  en la tabla "tarea"
                var result = connection.Insert(crear);
                if (result > 0)

                {
                    DisplayAlert("correcto", "la tarea se creo correctamente", "ok");
                }
                else
                {
                    DisplayAlert("Error", "La Tarea No Fue Creada", "ok");
                }

            }

        }
        async private void ListPersonsOne(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ListarPersonas());
        }


    }

}   